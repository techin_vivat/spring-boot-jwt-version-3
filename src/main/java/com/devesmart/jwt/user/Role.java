package com.devesmart.jwt.user;

public enum Role {
    USER,
    ADMIN
}
